package ManagedBeans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;


import model.Candidate;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import services.*;


@ManagedBean(name = "candidatesBean") 
@SessionScoped
public class CandidatesBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int act;
	private String address;
	private Date creationDate;
	private String email;
	private String firstName;
	private String introduction;
	private String lastName;
	private String password;
	private String phoneNumber;
	private String photo;
	private String skills;
	private String userName;
	private Candidate candidat;
	private List<Candidate> listcandidate;
	private int CandidateIdToBeUpdated;

	
	
	
	
	@EJB
	CandidateService SCand;
	
	@EJB
	CandidatesService s;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAct() {
		return act;
	}

	public void setAct(int act) {
		this.act = act;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Candidate getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidate candidat) {
		this.candidat = candidat;
	}
	public List<Candidate> getListcandidate() {
		return listcandidate=s.getAllCandidates();
	}


	public void setListcandidate(List<Candidate> listcandidate) {
		this.listcandidate = listcandidate;
	}


	public void removeCandidate(int id) {
		s.deleteCandidateById(id);
		
	}
	public String AddCandidate() {
		Candidate candidat = new Candidate();
		candidat.setAct(act);
		candidat.setAddress(address);
		candidat.setCreationDate(creationDate);
		candidat.setEmail(email);
		candidat.setFirstName(firstName);
		candidat.setIntroduction(introduction);
		candidat.setPassword(password);
		candidat.setPhoneNumber(phoneNumber);
		candidat.setPhoto(photo);
		candidat.setSkills(skills);
		candidat.setUserName(userName);
	s.ajouterCandidat(candidat);
	String navigateTo="loginCandidate?faces-redirect=true";
	return navigateTo;


	}
	public String UpdateClient() {
		String navigateTo = null;
		Candidate x = new Candidate(CandidateIdToBeUpdated,act, creationDate, address, email, firstName, introduction, lastName, password, phoneNumber, photo, skills, userName);
		s.modifierClient(x);
		navigateTo = "/modifier?faces-redirect=true";
		return navigateTo;

	}
	
	
	
}
