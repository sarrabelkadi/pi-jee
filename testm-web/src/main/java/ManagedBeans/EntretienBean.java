package ManagedBeans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;


import model.Candidature;
import model.Entretien;
import model.Candidate;

import javax.ejb.EJB;

import services.*;

@ManagedBean(name = "entretienBean") 
@SessionScoped
public class EntretienBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int entretienId;
	private int act;
	private String address;
	private Date dateEntretien;
	private Candidature candidate;
	
	
	private List<Entretien> listentretien;
	
	public static Entretien candidatconnect = null;
	
	@EJB
	EntretienService SCand;
	
	

public void AddEntretien() {
	Entretien candidat = new Entretien();
	candidat.setDateEntretien(dateEntretien);
	candidat.setCandidature(candidate);
	
SCand.ajouterEntretien(candidat);
}



public int getEntretienId() {
	return entretienId;
}



public void setEntretienId(int entretienId) {
	this.entretienId = entretienId;
}



public Date getDateEntretien() {
	return dateEntretien;
}



public void setDateEntretien(Date dateEntretien) {
	this.dateEntretien = dateEntretien;
}



public Candidature getCandidate() {
	return candidate;
}



public void setCandidate(Candidature candidate) {
	this.candidate = candidate;
}

}
