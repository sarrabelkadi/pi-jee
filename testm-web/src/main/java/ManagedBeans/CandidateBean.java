package ManagedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

import model.Candidate;
import services.CandidateService;
import services.CandidatesService;


@ManagedBean(name = "candidateBean") 
@SessionScoped
public class CandidateBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int act;
	private String address;
	private Date creationDate;
	private String email;
	private String firstName;
	private String introduction;
	private String lastName;
	private String password;
	private String phoneNumber;
	private String photo;
	private String skills;
	private String userName;
	private Candidate candidat;
	private List<Candidate> listcandidate=new ArrayList<Candidate>();
	private int CandidateIdToBeUpdated;
	
	
	public int getCandidateIdToBeUpdated() {
		return CandidateIdToBeUpdated;
	}


	public void setCandidateIdToBeUpdated(int candidateIdToBeUpdated) {
		CandidateIdToBeUpdated = candidateIdToBeUpdated;
	}
	public static Candidate candidatconnect = null;
	
	@EJB
	CandidateService SCand;
	@EJB
	CandidatesService m;
	public CandidateBean() {
		//super();
		System.out.println("*******************************************");
		System.out.println("id connecté  "+LoginBean.idconn);
		this.setCandidat(candidatconnect);
		System.out.println("*******************************************");
		System.out.println("candidat  "+candidatconnect.getEmail());
		/*candidat =new Candidate(); 
		System.out.println("******************           candidat =new Candidate();     *************************");
		this.setCandidat(getCandidate(1));
		System.out.println("*******************************************");
		System.out.println("candidat  "+candidat.getEmail());*/
	}
	

public String AddCandidate() {
	Candidate candidat = new Candidate();
	candidat.setAct(act);
	candidat.setAddress(address);
	candidat.setCreationDate(creationDate);
	candidat.setEmail(email);
	candidat.setFirstName(firstName);
	candidat.setIntroduction(introduction);
	candidat.setPassword(password);
	candidat.setPhoneNumber(phoneNumber);
	candidat.setPhoto(photo);
	candidat.setSkills(skills);
	candidat.setUserName(userName);
SCand.ajouterCandidat(candidat);
String navigateTo="loginCandidate?faces-redirect=true";
return navigateTo;


}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAct() {
		return act;
	}

	public void setAct(int act) {
		this.act = act;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Candidate getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidate candidat) {
		this.candidat = candidat;
	}
	public List<Candidate> getListcandidate() {
		return listcandidate;
	}


	public void setListcandidate(List<Candidate> listcandidate) {
		this.listcandidate = listcandidate;
	}


	
	public static Candidate getCandidatconnect() {
		return candidatconnect;
	}

	public static void setCandidatconnect(Candidate candidatconnect) {
		CandidateBean.candidatconnect = candidatconnect;
	}

	public Candidate getCandidate(int id) {
		candidat = SCand.DisplayCandidate(id);
		System.out.println("candidat candidateBean "+candidat.getEmail());
		return candidat;
		} 
	public void mettreAjourCandidat() {
SCand.updateCandidate(new Candidate( CandidateIdToBeUpdated,act, creationDate, address, email, firstName, introduction, lastName, password, phoneNumber, photo, skills, userName));
   }
	public void modifier(Candidate candidat) {
		
		  this.setAddress(candidat.getAddress()); 
		  this.setCreationDate(candidat.getCreationDate());
		  this.setEmail(candidat.getEmail()); 
		  this.setFirstName(candidat.getFirstName()); 
		  this.setIntroduction(candidat.getIntroduction()); 
		  this.setLastName(candidat.getLastName()); 
		  this.setPassword(candidat.getPassword()); 
		  this.setPhoneNumber(candidat.getPhoneNumber()); 
		  this.setPhoto(candidat.getPhoto()); 
		  this.setSkills(candidat.getSkills());
		  this.setUserName(candidat.getUserName());
		  this.setCandidateIdToBeUpdated(candidat.getId()); 
		 
		  }
	public String removeCandidate(int id) {
		SCand.deleteCandidateById(id);
		String navigateTo="/webapp/loginCandidate?faces-redirect=true";
		return navigateTo;
	}
	
	
}

