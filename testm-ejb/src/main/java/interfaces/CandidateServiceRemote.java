package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.*;

@Remote
public interface CandidateServiceRemote {

	public Candidate DisplayCandidate (int id);
	public Candidate getByEmailPwd (String email, String pwd); 
	public void ajouterCandidat(Candidate candidat);
	public void updateCandidate(Candidate p);
	public List<Candidate> getAllCandidates();
	public void deleteCandidateById(int id);
	


}
