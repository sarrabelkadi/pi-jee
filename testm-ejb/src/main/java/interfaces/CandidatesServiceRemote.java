package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.*;
@Remote
public interface CandidatesServiceRemote {
	
public List<Candidate>getAllCandidates();
public void deleteCandidateById(int id);
public void ajouterCandidat(Candidate candidat);
public void modifierClient(Candidate candidate);
}
