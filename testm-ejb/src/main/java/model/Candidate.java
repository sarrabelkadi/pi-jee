package model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Candidates database table.
 * 
 */
@Entity
@Table(name="Candidates")
@NamedQuery(name="Candidate.findAll", query="SELECT c FROM Candidate c")
public class Candidate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	private int act;

	private String address;

	private Date creationDate;

	private String email;
	private String firstName;

	private String introduction;

	private String lastName;

	private String password;

	private String phoneNumber;

	private String photo;

	private String skills;

	private String userName;

	//bi-directional many-to-one association to Candidature
	@OneToMany(mappedBy="candidate")
	private List<Candidature> candidatures;

	//bi-directional many-to-one association to Certification
	@OneToMany(mappedBy="candidate")
	private List<Certification> certifications;

	//bi-directional many-to-one association to Contact
	//@OneToMany(mappedBy="candidate")
	//private List<Contact> contacts;

	//bi-directional many-to-one association to Education
	@OneToMany(mappedBy="candidate")
	private List<Education> educations;

	//bi-directional many-to-one association to Experience
	@OneToMany(mappedBy="candidate")
	private List<Experience> experiences;

	public Candidate() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAct() {
		return this.act;
	}

	public void setAct(int act) {
		this.act = act;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIntroduction() {
		return this.introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSkills() {
		return this.skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<Candidature> getCandidatures() {
		return this.candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public Candidature addCandidature(Candidature candidature) {
		getCandidatures().add(candidature);
		candidature.setCandidate(this);

		return candidature;
	}

	public Candidature removeCandidature(Candidature candidature) {
		getCandidatures().remove(candidature);
		candidature.setCandidate(null);

		return candidature;
	}

	public List<Certification> getCertifications() {
		return this.certifications;
	}

	public void setCertifications(List<Certification> certifications) {
		this.certifications = certifications;
	}

	public Certification addCertification(Certification certification) {
		getCertifications().add(certification);
		certification.setCandidate(this);

		return certification;
	}

	public Certification removeCertification(Certification certification) {
		getCertifications().remove(certification);
		certification.setCandidate(null);

		return certification;
	}

/*	public List<Contact> getContacts() {
		return this.contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}*/

/*	public Contact addContact(Contact contact) {
		getContacts().add(contact);
		contact.setCandidate(this);

		return contact;
	}

	public Contact removeContact(Contact contact) {
		getContacts().remove(contact);
		contact.setCandidate(null);

		return contact;
	}*/

	public List<Education> getEducations() {
		return this.educations;
	}

	public void setEducations(List<Education> educations) {
		this.educations = educations;
	}

	public Education addEducation(Education education) {
		getEducations().add(education);
		education.setCandidate(this);

		return education;
	}

	public Education removeEducation(Education education) {
		getEducations().remove(education);
		education.setCandidate(null);

		return education;
	}

	public List<Experience> getExperiences() {
		return this.experiences;
	}

	public void setExperiences(List<Experience> experiences) {
		this.experiences = experiences;
	}

	public Experience addExperience(Experience experience) {
		getExperiences().add(experience);
		experience.setCandidate(this);

		return experience;
	}

	public Experience removeExperience(Experience experience) {
		getExperiences().remove(experience);
		experience.setCandidate(null);

		return experience;
	}
	public Candidate(int id,int act,Date creationDate, String address,String email, String firstName, String introduction,String lastName,String Password,String phoneNumber,String photo,String skills,String userName) {
		super();
		this.id = id;

		this.act = act;
		this.creationDate = creationDate;
		this.address = address;
		this.email = email;
		this.firstName = firstName;
		this.introduction = introduction;
		this.lastName = lastName;
		this.password = Password;
		this.phoneNumber = phoneNumber;
		this.photo = photo;
		this.skills = skills;
		this.userName = userName;
		
	}

	@Override
	public String toString() {
		return "Candidate [id=" + id + ", act=" + act + ", address=" + address + ", creationDate=" + creationDate
				+ ", email=" + email + ", firstName=" + firstName + ", introduction=" + introduction + ", lastName="
				+ lastName + ", password=" + password + ", phoneNumber=" + phoneNumber + ", photo=" + photo
				+ ", skills=" + skills + ", userName=" + userName + ", candidatures=" + candidatures
				+ ", certifications=" + certifications + ", educations=" + educations + ", experiences=" + experiences
				+ "]";
	}
	
}