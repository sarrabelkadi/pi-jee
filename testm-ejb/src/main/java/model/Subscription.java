package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Subscriptions database table.
 * 
 */
@Entity
@Table(name="Subscriptions")
@NamedQuery(name="Subscription.findAll", query="SELECT s FROM Subscription s")
public class Subscription implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SubscriptionPK id;

	public Subscription() {
	}

	public SubscriptionPK getId() {
		return this.id;
	}

	public void setId(SubscriptionPK id) {
		this.id = id;
	}

}