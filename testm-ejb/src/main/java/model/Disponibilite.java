package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Disponibilites database table.
 * 
 */
@Entity
@Table(name="Disponibilites")
@NamedQuery(name="Disponibilite.findAll", query="SELECT d FROM Disponibilite d")
public class Disponibilite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	private String datedis;

	public Disponibilite() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDatedis() {
		return this.datedis;
	}

	public void setDatedis(String datedis) {
		this.datedis = datedis;
	}

}