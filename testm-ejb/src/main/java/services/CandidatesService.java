package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import interfaces.CandidatesServiceRemote;
import model.Candidate;



@Stateless
@LocalBean
public class CandidatesService implements  CandidatesServiceRemote {
	
	@PersistenceContext(unitName="primary")
	EntityManager em;

	
	public List<Candidate> getAllCandidates() {
		List<Candidate> emp = em.createQuery("Select p from Candidate p", Candidate.class).getResultList();
		for(Candidate x: emp)
			{System.out.println("candidates list !!");
			System.out.println(x);
			}
		return emp;
	}
	public void deleteCandidateById(int id) {Candidate p = em.find(Candidate.class,id); em.remove(p); }
	@Override
	public void ajouterCandidat(Candidate candidat) {
	    em.persist(candidat);
		}
	public void modifierClient(Candidate candidate) {
		em.merge(candidate);
		}
}
