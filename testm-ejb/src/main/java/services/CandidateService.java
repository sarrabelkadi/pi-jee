package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import interfaces.CandidateServiceLocal;
import interfaces.CandidateServiceRemote;
import model.Candidate;


@Stateless
@LocalBean
public class CandidateService implements CandidateServiceLocal, CandidateServiceRemote {
	
	@PersistenceContext(unitName="primary")
	EntityManager em;

	@Override
	public Candidate DisplayCandidate(int id) {
		System.out.println("****************"+em.find(Candidate.class, id).getEmail());
		TypedQuery<Candidate> query =  em.createQuery("Select c from Candidate c WHERE c.id=:id", Candidate.class);
		query.setParameter("id", id); 
		Candidate candidat = null; 
		try {
			candidat = query.getSingleResult(); 
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return candidat;
	}

	@Override
	public Candidate getByEmailPwd(String email, String pwd) {
		TypedQuery<Candidate> query = 
				em.createQuery("select c from Candidate c WHERE c.email=:email and c.password=:pwd ", Candidate.class); 
				query.setParameter("email", email); 
				query.setParameter("pwd", pwd); 
				Candidate candidat = null; 
				try {
					candidat = query.getSingleResult(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				return candidat;
	}
	public List<Candidate> res(String im) {
		List<Candidate> query = em.createQuery("select c from Candidate c WHERE c.email=:email", Candidate.class).setParameter("email", im).getResultList();
				return query;
	}
	@Override
	public void ajouterCandidat(Candidate candidat) {
	    em.merge(candidat);
	    em.flush();
		}
	public List<Candidate> getAllCandidates() {
		List<Candidate> emp = em.createQuery("Select p from Candidate p", Candidate.class).getResultList();
		return emp;
	}
	public void updateCandidate(Candidate p) { 

		
	
	em.persist(p);
	}
	public void deleteCandidateById(int id) {Candidate p = em.find(Candidate.class,id); em.remove(p); }
	
}
