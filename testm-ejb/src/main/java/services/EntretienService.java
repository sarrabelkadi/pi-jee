package services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import interfaces.CandidateServiceLocal;
import interfaces.EntretienServiceRemote;
import model.Entretien;

@Stateless
@LocalBean
public class EntretienService implements  EntretienServiceRemote  {
	@PersistenceContext(unitName="primary")
	EntityManager em;
	@Override
	public void ajouterEntretien(Entretien candidat) {
	    em.persist(candidat);
		}

}
